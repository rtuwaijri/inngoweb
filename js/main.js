var app = {};

app.init = function init() {
	app.openMenu();
	app.mailSubscribe();
	app.datePicker();
	app.scrollPage();
	app.viewMore();
	app.maps();
	app.showOptions();
};

$.fn.onEnterKey = function(closure) {
    $(this).keypress(function(event) {
        var code = event.keyCode ? event.keyCode : event.which;

        if (code == 13) {
            closure();
            return false;
        }
    });
};

app.openMenu = function() {
	var c = 0;
	$('.hamburger').off().on({
		'click' : function() {
			var $this = $(this),
				$modal = $('#modal'),
				docHeight = $(document).height(),
				posOffset = $('.open-modal').position(),
				top;

			if($('body').find('#menu').length > 0) {
				top = '45px';
			} else {
				top = posOffset.top;
			}

			$modal.find('.fundo').css('height', docHeight);

			if(c % 2 == 0) {
				$this.addClass('is-active');
				$modal.show().animate({'top' : '0'}, 'fast', 'linear').find('.hamburger').css({'left' : posOffset.left, 'top' : top});
			} else {
				$('.hamburger').removeClass('is-active');
				$modal.animate({'top' : '100vh'}, 'fast', 'linear', function() {
					$modal.hide().find('.hamburger').addClass('is-active');
				});
			}

			c++;

			$(window).resize(function() {
				var $menu = $('.open-modal'),
					posOffset = $menu.position();

				$modal.find('.hamburger').css('left', posOffset.left);
			});
		}
	});
};

app.mailSubscribe = function() {
	$('#mailchimp input').onEnterKey(function() {
		$('#mailchimp').submit();
		return false;
	});
};

app.datePicker = function() {
	$('.datepicker').pickadate({
		format: 'dd mmm, yyyy',
	    selectMonths: true,
	    selectYears: 15
	});
};

app.scrollPage = function() {
	var winHeight = $(window).height()+70,
		body = $('html, body');

	$('.arrow-scroll').off().on({
		'click' : function() {
			body.stop().animate({scrollTop: winHeight}, '300', 'swing');
		}
	});
};

app.viewMore = function() {
	$('.view-more').off().on({
		'click' : function() {
			var $this = $(this),
				$card = $('.card'),
				pos = $this.parents('.card').index();

			$('.card').stop().animate({'width' : '49%'}, 'fast', 'linear');

			if($card.eq(pos).find('.extended').is(':visible')) {
				$card.eq(pos).stop().animate({'width' : '49%'}, 'fast', 'linear').find('img').stop().animate({'width' : '627px'}, 'fast', 'linear');
				$card.eq(pos).find('.extended').fadeOut(function() {
					$card.eq(pos).find('.name').fadeIn();
				});
			} else {
				$card.eq(pos).stop().animate({'width' : '100%'}, 'fast', 'linear').find('img').css('width', '1280px');
				$card.eq(pos).find('.name').fadeOut(function() {
					$card.eq(pos).find('.extended').fadeIn();
				});
			}

			$('.name').show();
			$('.extended').hide();
		}
	});
};

app.maps = function() {
	$('.maps').off().on({
		'click' : function() {
			$('.maps iframe').css('pointer-events', 'auto');
		},
		'mouseleave' : function() {
			$('.maps iframe').css('pointer-events', 'none');
		}
	});
};

app.showOptions = function() {
	$('.open-options').off().on({
		'click' : function() {
			var showOptions = $(this).parents('.block-room').find('.show-options');

			if(showOptions.is(':visible')) {
				showOptions.slideUp('150');
				$(this).removeClass('active').find('span').text('Options').siblings('.material-icons').show();
			} else {
				showOptions.slideDown('150');
				$(this).addClass('active').find('span').text('Show Less').siblings('.material-icons').hide();
			}
		}
	});
};

app.payment = function() {
	var $cardNumber = $('#card-number');

	var cards = {
	    visa: /^4[0-9]{12}(?:[0-9]{3})/,
	    mastercard: /^5[1-5][0-9]{14}/,
	    amex: /^3[47][0-9]{13}/
	};

	function validCC(nr, cards) {
        for (window.card in cards) {
        	if (nr.match(cards[card])) {
        		$cardNumber.removeClass('invalid').addClass('valid');
        		if(card != $('.card-type.active').data('type')) {
		        	alert('CREDIT CARD INVALID!');
        		}
        		return card;
        	} else {
		        alert('CREDIT CARD INVALID!');
		   		setTimeout(function() { $cardNumber.removeClass('valid').addClass('invalid'); }, 300);
		        return false;
        	}
        }
    }

	$cardNumber.off().on({
		'focusout' : function() {
			validCC($cardNumber.val(), cards);
		}
	});


	$('.card-type').off().on({
		'click' : function() {
			$(this).nextAll().each(function () {
				$(this).removeClass('active');
			});
			$(this).prevAll().each(function () {
				$(this).removeClass('active');
			});

			$(this).addClass('active');

			if($cardNumber.val() != '') {
				validCC($cardNumber.val(), cards);
			}
		}
	});

	$('.checkbox').off().on({
		'click' : function() {
			var checked = $('.checked');

			$('#agree').attr('checked', 'checked');
			if(checked.is(':visible')) { checked.hide(); } else { checked.show(); }
		}
	});

	$('button').off().on({
		'click' : function() {
			var $required = $('.big-block input:required'),
				errors = false;

			if(!$('#agree').is(':checked')) {
				alert('You must to read and agree with the Booking Conditions and Privacy Police!');
			} else {
				$required.map(function(){
			        if($required.val() && ($required.val() != 'Month' || $required.val() != 'Year')) {
			            $required.removeClass('invalid');
			        } else {
			            $required.addClass('invalid');
			            errors = true;
			        }
			    });

			    if(errors) {
			    	alert('You must to complete all fields!');
			    } else {
			    	alert('Success!');
			    }
			}
		}
	})
};

app.init();