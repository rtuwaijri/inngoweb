<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
	    <meta name="viewport" content="width=1200, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<div id="hero" class="landing">
			<div class="content">
				<div class="row">
					<div class="main-text">
						<div class="center-block center-align">
							<img src="../dist/img/inn-go-hotel-golden--big.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
						</div>						
					</div>
					<div class="center-block box-scroll">
						<p class="f20 dark-golden center-align a-lightitalic">Scroll to discover</p>
						<a href="javascript:;" class="arrow-scroll bounce icon"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="intern-landing" class="clearfix">
			<div class="container full-h">
				<div class="header-landing clearfix">
					<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
				</div>
				<h1 class="f180 av-roman center-align article dark-golden">We build  amazing hotels</h1>
			</div>
			<div class="container full-h">
				<div class="header-landing clearfix">
					<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
					<a href="media-center.php" class="right av-roman f18 golden">رجال الإعلام Center</a>
				</div>
				<h2 class="f18 av-roman center-align article article--subtitle article--title dark-golden">Kuwait City</h2>
				<h3 class="f87 av-roman center-align article article--title dark-golden">Kuwait Plaza Hotel</h3>
				<div class="clearfix">
					<div class="col m5">
						<img class="responsive-img" src="http://placehold.it/600x490">
					</div>
					<div class="col m7">
						<div class="col m11 center-block">
							<p class="f22 a-light dark-golden">A brillant experience for business exevutives and casual travelerers visiting Kuwait Cities. World-class facilities, a staff prepared to serve customer's needs with excellence and fantastic reviews, inn & Go is a go-to destination.</p>
							<a href="javascript:;" class="f20 a-light btn-large btn-large--landing ">Discover &ndash;&rsaquo;</a>
						</div>
					</div>
				</div>
			</div>
			<div class="container full-h">
				<div class="header-landing clearfix">
					<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
				</div>
				<h2 class="f18 av-roman center-align article article--subtitle dark-golden">معلومات عنا</h2>
				<h3 class="f65 av-roman center-align article article--title article--subtitle dark-golden">We are a Kwaiti company commited to creating world-class hotels. We belive leisure is an art and our customers live remarkable experiences</h3>
				<div class="clearfix">
					<div class="col m5 right">
						<div class="col m12 center-block">
							<p class="f22 a-light dark-golden">Born in 1995 and now present in more than 15 destinations, we have built a story around great services that impress people.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container full-h">
				<div class="header-landing clearfix">
					<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
				</div>
				<h2 class="f18 av-roman center-align article article--subtitle dark-golden">معلومات عنا</h2>
				<h3 class="f65 av-roman center-align article article--title dark-golden">Updates and news about Inn & Go. For press inquiries, pr@innandgo.com</h3>
				<div class="clearfix">
					<div class="col m7">
						<!-- Articles -->
					</div>
					<div class="col m5">
						<!-- Tweets -->
					</div>
				</div>
			</div>
			<div class="container">
				<div class="header-landing clearfix">
					<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
				</div>
				<h2 class="f65 av-roman center-align article article--subtitle dark-golden">Want to contact us?</h2>
				<div class="clearfix">
					<form id="contact" class="col m6 center-block clearfix" method="post">
						<div class="input-field clearfix">
							<input placeholder="Name" id="name" name="name" type="text" class="validate a-light f22" required="">
							<label for="name" data-error="خاطئ" class="active"></label>
						</div>
						<div class="input-field clearfix">
							<input placeholder="Email" id="email" name="email" type="email" class="validate a-light f22" required="">
							<label for="email" data-error="خاطئ" class="active"></label>
						</div>
						<div class="input-field clearfix">
							<textarea placeholder="What's your message?" id="message" name="message" class="validate a-light f22 materialize-textarea" required=""></textarea>
							<label for="message" data-error="خاطئ" class="active"></label>
						</div>
						<div class="center-align">
							<input type="submit" value="الحصول على اتصال" class="f22 a-regular btn-large btn-large--landing">
						</div>
					</form>
				</div>
			</div>
		</div>

		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m2">
					<a href="./" class="hd-medium f44 white-text">الصفحة الرئيسية</a>
					<a href="javascript:;" class="hd-medium f44 white-text">معلومات عنا</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أماكن الإقامة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">المطاعم</a>
					<a href="javascript:;" class="hd-medium f44 white-text">مرافق</a>
					<a href="javascript:;" class="hd-medium f44 white-text">خدمات</a>
					<a href="javascript:;" class="hd-medium f44 white-text">اتصل بنا</a>
				</div>
				<div class="text-right col m2">
					<a href="javascript:;" class="hd-medium f44 white-text">خبرة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أحداث</a>
					<a href="javascript:;" class="hd-medium f44 white-text">رجال الإعلام</a>
					<div class="newsletter">
						<p class="white-text f18 a-light">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<div class="input-field col s12">
								<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light">
							</div>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>