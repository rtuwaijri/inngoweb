<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<div id="hero">
			<div class="content">
				<div class="clearfix">
					<a href="javascript:;" class="logo left"><img src="../dist/img/inn-go-logo.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
					<div class="right block-menu">
						<p class="lang left a-light white-text f15"><u>Ar</u></p>
						<button class="hamburger hamburger--squeeze open-modal right" type="button" aria-label="Menu" aria-controls="navigation">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="row main-text clearfix">
					<div class="col m7 center-block">
						<h2 class="f60 white-text center-align hd-medium">اتصل بنا</h2>
					</div>
				</div>
				<div class="row">
					<div class="center-block box-scroll">
						<p class="f20 white-text center-align a-lightitalic">Scroll to discover</p>
						<a href="javascript:;" class="arrow-scroll bounce icon"></a>
					</div>
				</div>
				<div class="row">
					<div class="check-booking">
						<div class="clearfix left">
							<div class="left calendar">
								<input type="date" name="checkin" class="checkin datepicker" value="تحقق في">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
							<div class="left calendar">
								<input type="date" name="checkout" class="checkout datepicker" value="الدفع">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
						</div>
						<a href="javascript:;" class="btn f16 a-regular white-text left">التحقق من توافر</a>
					</div>
				</div>
			</div>
		</div>
		<div id="contact-map" class="clearfix">
			<div class="maps">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16564.522944409677!2d47.90512084797162!3d29.333935697087913!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x10eea32e7d0e66ce!2sKuwait+Institute+for+Scientific+Research+(KISR)!5e0!3m2!1spt-BR!2sbr!4v1458049991615" width="100%" height="1200" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="box-address col m5 black">
				<div class="col m8 center-block">
					<p class="city white-text f144 hd-medium">Kuwait City</p>
					<p class="f18 a-light white-text">Inn and Go Plaza Kuwait is a mere 20 minutes from the airport and in the vicinity of The Grand Mosque of Kuwait, Kuwait Tower, Liberation Tower, and the Scientific Museum. Here for work? Enjoy high-speed internet access, meeting facilities and a ballroom for functions that can hold up to 250 guests.</p>
					<div class="col m6 left">
						<p class="f18 a-light white-text">Inn & Go - Plaza Hotel</p>
						<p class="f18 a-light white-text">City here, Kuwait</p>
						<p class="f18 a-light white-text">Address Here, 123</p>
						<p class="f18 a-light white-text">14295 - Zipcode</p>
					</div>
					<div class="col m6 right">
						<p class="f18 a-light white-text">T. +39 8765 478906</p>
						<p class="f18 a-light white-text">T. +94 2839 824950</p>
						<p class="f18 a-light white-text">M. talk@innandgo.com</p>
					</div>
					<div class="social-media clearfix">
						<a href="javascript:;" class="left facebook icon"></a>
						<a href="javascript:;" class="left twitter icon"></a>
						<a href="javascript:;" class="left instagram icon"></a>
					</div>
				</div>
			</div>
			<div class="get-touch container">
				<p class="f200 hd-medium">الحصول على اتصال</p>
				<div class="col m7 center-block">
					<p class="f22 a-light center-align">Inn and Go Plaza Kuwait is a mere 20 minutes from the airport and in the vicinity of The Grand Mosque of Kuwait, Kuwait Tower, Liberation Tower, and the Scientific Museum. Here for work? Enjoy high-speed internet access, meeting facilities and a ballroom for functions that can hold up to 250 guests.</p>
				</div>
				<form id="contact" class="col m6 center-block clearfix" method="post">
					<div class="input-field clearfix">
						<input placeholder="اسمك" id="name" name="name" type="text" class="validate a-light f22" required>
						<label for="name" data-error="خاطئ"></label>
					</div>
					<div class="input-field clearfix">
						<input placeholder="بريدك الالكتروني" id="email" name="email" type="email" class="validate a-light f22" required>
						<label for="email" data-error="خاطئ"></label>
					</div>
					<div class="input-field clearfix">
						<textarea placeholder="رسالتك. كيف يمكننا مساعدتك؟" id="message" name="message" class="validate a-light f22" required></textarea>
						<label for="message" data-error="خاطئ"></label>
					</div>
					<input type="submit" value="Send" class="f22 a-regular btn">
				</form>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m2">
					<a href="./" class="hd-medium f44 white-text">الصفحة الرئيسية</a>
					<a href="javascript:;" class="hd-medium f44 white-text">معلومات عنا</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أماكن الإقامة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">المطاعم</a>
					<a href="javascript:;" class="hd-medium f44 white-text">مرافق</a>
					<a href="javascript:;" class="hd-medium f44 white-text">خدمات</a>
					<a href="javascript:;" class="hd-medium f44 white-text active">اتصل بنا</a>
				</div>
				<div class="text-right col m2">
					<a href="javascript:;" class="hd-medium f44 white-text">خبرة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أحداث</a>
					<a href="javascript:;" class="hd-medium f44 white-text">رجال الإعلام</a>
					<div class="newsletter">
						<p class="white-text f18 a-light">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light" required>
									<label for="email" data-error="خاطئ"></label>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>