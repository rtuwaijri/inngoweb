<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<div id="hero">
			<div class="content">
				<div class="clearfix">
					<a href="javascript:;" class="logo left"><img src="../dist/img/inn-go-logo.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza"></a>
					<div class="right block-menu">
						<p class="lang left a-light white-text f15"><u>Ar</u></p>
						<button class="hamburger hamburger--squeeze open-modal right" type="button" aria-label="Menu" aria-controls="navigation">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="row main-text clearfix">
					<div class="col m7 center-block">
						<h2 class="f60 white-text center-align hd-medium">الاجتماعات والمؤتمرات</h2>
					</div>
				</div>
				<div class="row">
					<div class="center-block box-scroll">
						<p class="f20 white-text center-align a-lightitalic">Scroll to discover</p>
						<a href="javascript:;" class="arrow-scroll bounce icon"></a>
					</div>
				</div>
				<div class="row">
					<div class="check-booking">
						<div class="clearfix left">
							<div class="left calendar">
								<input type="date" name="checkin" class="checkin datepicker" value="تحقق في">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
							<div class="left calendar">
								<input type="date" name="checkout" class="checkout datepicker" value="الدفع">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
						</div>
						<a href="javascript:;" class="btn f16 a-regular white-text left">التحقق من توافر</a>
					</div>
				</div>
			</div>
		</div>
		<div id="intern" class="clearfix events">
			<div class="container clearfix">
				<div class="col m7 info">
					<h1 class="f200 hd-medium">أحداث</h1>
					<div class="col m11 text-events">
						<p class="f22 a-light">
							Inn and Go Plaza Kuwait is a mere 20 minutes from the airport and in the vicinity of The Grand Mosque of Kuwait, Kuwait Tower, Liberation Tower, and the Scientific Museum. Here for work? Enjoy high-speed internet access, meeting facilities and a ballroom for functions that can hold up to 250 guests.
						</p>
					</div>
				</div>
			</div>
			<img class="responsive-img full-image" src="http://placehold.it/1920x760">
			<div class="container clearfix">
				<div class="col m7 center-block">
					<h2 class="f60 hd-medium center-align">لماذا أخترتنا؟</h2>
					<div class="text">
						<p class="f18 a-light">
							Each Member purchases one week in high season which remains the same each year for the duration of the lease. Each Member is also entitled to four planned vacation weeks throughout the year which are chosen several months in advance. To avoid conflicts, an equitable rotation system is used. The lease is for a period of 75 years and Members can sell or transfer their membership anytime.
						</p>
						<p class="f18 a-light">
							For any weeks that are not occupied by the Member, friends or family, these can be released back to the hotel for rental. Each member receives 60% of the rental and this money is credited to the Club Member’s account. This money can be used to pay for extra's incurred at the hotel (car rental, flights, bar and restaurant bills, etc.), pay the quarterly due fees or can be credited back to the Member.
						</p>
						<p class="f18 a-light">
							The Club also provides storage for all Members. Personal belongings can be left in special containers in the hotel’s secure air-conditioned storage facility. The boxes will be delivered to the villa prior to arrival and taken away at the end of the stay. Beach Club: umbrellas, beach towels, snorkelling gear, water activities, beverage services and reserved beach chairs.
						</p>
					</div>
				</div>
				<div class="our-events clearfix">
					<div class="col m6 left">
						<div class="col m11 center-block">
							<h2 class="f58 hd-medium">أحداثنا الغرفة</h2>
							<p class="f18 a-light">
								Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes, 24 hours in- room dining, laundry service, safety deposit box.
							</p>
							<a href="javascript:;" class="btn white-text f20 a-light">Learn More</a>
						</div>
					</div>
					<div class="col m6 right">
						<img class="responsive-img" src="http://placehold.it/659x539">
						<div class="col m11 center-block">
							<h2 class="f60 hd-medium">Business Suite</h2>
							<p class="f18 a-light">
								High speed internet access, Satellite TV including international channels, in-room safe, individually controlled air conditioning system, IDD telephone line, coffee & tea facilities, mini bar, daily news papers and hair dryer. In-room personal computer included in certain rooms.
							</p>
						</div>
					</div>
					<div class="col m6 left">
						<img class="responsive-img" src="http://placehold.it/659x539">
						<div class="col m11 center-block">
							<h2 class="f60 hd-medium">Deluxe</h2>
							<p class="f18 a-light">
								Deluxe rooms come with a range of upgraded value-added benefits such as Personal Computer in the room to suit business or leisure travelers. In addition, rooms on the 10th floor are equipped with extra toiletries, bathtub, and separate shower.
							</p>
						</div>
					</div>
					<div class="col m6 right last">
						<div class="col m12 center-block">
							<img class="responsive-img" src="http://placehold.it/599x737">
						</div>
						<div class="col m11 center-block">
							<h2 class="f60 hd-medium">Executive</h2>
							<p class="f18 a-light">
								Each of our Executive rooms is thoughtfully designed for both the business and leisure traveler. Large bathrooms with shower and Jacuzzi are also available.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="quotes clearfix">
				<div class="col m6 right">
					<div class="col m10 valign-wrapper right">
						<p class="f180 hd-light valign">اقتبس</p>
					</div>
				</div>
				<div class="col m6 box left">
					<div class="col m8 center-block">
						<form id="quotes" method="post">
							<div class="input-field col m12">
								<select>
									<option value="" disabled selected>حدد نوع الحدث</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
		 					</div>
							<div class="input-field col m12">
								<input type="tel" id="atendees" name="atendees" class="f22 a-light" value="Expected Atendees" required>
							</div>
							<div class="input-field col m12">
								<p class="col m12 f19 a-light"><i>تواريخ الحدث</i></p>
								<div class="box-date col m5 left">
									<input type="text" id="start" name="start" class="f22 a-light datepicker col m12" value="بداية" required>
									<span class="caret"></span>
								</div>
								<div class="box-date col m5 left">
									<input type="text" id="end" name="end" class="f22 a-light datepicker col m12" value="النهاية" required>
									<span class="caret"></span>
								</div>
							</div>
							<div class="input-field col m12">
								<input type="text" id="name" name="name" class="f22 a-light" value="اسمك" required>
							</div>
							<div class="input-field col m12">
								<input type="text" id="name" name="name" class="f22 a-light" value="شركة / منظمة" required>
							</div>
							<div class="input-field col m12">
								<input type="email" id="email" name="email" class="f22 a-light" value="البريد الإلكتروني" required>
								<label for="email" data-error="خاطئ"></label>
							</div>
							<div class="input-field col m12">
								<input type="tel" id="tel" name="tel" class="f22 a-light" value="رقم الهاتف" required>
							</div>
							<div class="input-field col m12">
								<input type="text" id="additional" name="additional" class="f22 a-light" value="معلومات إضافية (اختياري)"/>
							</div>
							<br clear="both">
							<input type="submit" value="احصل على السعر الآن" class="btn-large f20 white-text center-block" />
						</form>
	 				</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m2">
					<a href="./" class="hd-medium f44 white-text active">الصفحة الرئيسية</a>
					<a href="javascript:;" class="hd-medium f44 white-text">معلومات عنا</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أماكن الإقامة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">المطاعم</a>
					<a href="javascript:;" class="hd-medium f44 white-text">مرافق</a>
					<a href="javascript:;" class="hd-medium f44 white-text">خدمات</a>
					<a href="javascript:;" class="hd-medium f44 white-text">اتصل بنا</a>
				</div>
				<div class="text-right col m2">
					<a href="javascript:;" class="hd-medium f44 white-text">خبرة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أحداث</a>
					<a href="javascript:;" class="hd-medium f44 white-text">رجال الإعلام</a>
					<div class="newsletter">
						<p class="white-text f18 a-light">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light" required>
									<label for="email" data-error="خاطئ"></label>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>$('select').material_select();</script>
	</body>
</html>