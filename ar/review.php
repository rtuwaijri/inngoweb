<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two" class="no-booking">
		<?php include_once('includes/header.php'); ?>
		<div id="booking" class="clearfix black">
			<div class="container">
				<div class="col m7 center-block">
					<ul class="clearfix">
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">1. حدد غرفة</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">2. دفع</a></li>
						<li class="f24 left active"><a href="javascript:;" class="hd-light white-text">3. مراجعة</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">4. التأكيد</a></li>
					</ul>
				</div>
				<p class="hd-medium f124 white-text center-align"><i></i>مراجعة</p>
			</div>
		</div>
		<div class="container clearfix">
			<div class="box-reservation col m9 center-block">
				<div class="col m7 s11 center-block">
					<h1 class="f94 hd-medium center-align">الحجز الخاص بك</h1>
					<div class="text">
						<p class="f18 a-light">
							Inn and Go Plaza Kuwait is a mere 20 minutes from the airport and in the vicinity of The Grand Mosque of Kuwait, Kuwait Tower, Liberation Tower, and the Scientific Museum. Here for work? Enjoy high-speed internet access, meeting facilities and a ballroom for functions that can hold up to 250 guests.
						</p>
					</div>
					<p class="f20 a-regular black-text"><b>Rakan Jordan Smith</b></p>
					<p class="f20 a-regular black-text">rakansmith@gmail.com</p>
					<p class="f20 a-regular black-text"><b>129 5th Avenue New York 78701</b></p>
					<p class="f20 a-regular black-text">129-398-239</p>
					<p class="f20 a-regular black-text"><b>American Express ending in 8923</b></p>
					<p class="f20 a-regular black-text">rakanjordan@gmail.com</p>
					<p class="f20 a-regular black-text"><b>Flight Number A938</b></p>
					<a href="../tpls/" class="btn"><span class="left">أكد</span> <i class="material-icons">trending_flat</i></a>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m2">
					<a href="./" class="hd-medium f44 white-text">الصفحة الرئيسية</a>
					<a href="javascript:;" class="hd-medium f44 white-text">معلومات عنا</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أماكن الإقامة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">المطاعم</a>
					<a href="javascript:;" class="hd-medium f44 white-text">مرافق</a>
					<a href="javascript:;" class="hd-medium f44 white-text">خدمات</a>
					<a href="javascript:;" class="hd-medium f44 white-text">اتصل بنا</a>
				</div>
				<div class="text-right col m2">
					<a href="javascript:;" class="hd-medium f44 white-text">خبرة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أحداث</a>
					<a href="javascript:;" class="hd-medium f44 white-text">رجال الإعلام</a>
					<div class="newsletter">
						<p class="white-text f18 a-light">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light" required>
									<label for="email" data-error="خاطئ"></label>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>