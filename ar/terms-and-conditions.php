<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<?php include_once('includes/header.php'); ?>
		<div id="intern" class="clearfix">
			<div class="container">
				<div class="col m7 center-block privacy">
					<h1 class="f60 hd-medium center-align">الأحكام والشروط</h1>
					<div class="text">
						<p>
							Each Member purchases one week in high season which remains the same each year for the duration of the lease. Each Member is also entitled to four planned vacation weeks throughout the year which are chosen several months in advance. To avoid conflicts, an equitable rotation system is used. The lease is for a period of 75 years and Members can sell or transfer their membership anytime.
						</p>
						<p>
							For any weeks that are not occupied by the Member, friends or family, these can be released back to the hotel for rental. Each member receives 60% of the rental and this money is credited to the Club Member’s account. This money can be used to pay for extra's incurred at the hotel (car rental, flights, bar and restaurant bills, etc.), pay the quarterly due fees or can be credited back to the Member.
						</p>
						<h2 class="hd-medium f30">1.0 Terms of Use</h2>
						<p>
							The Club provides a full time concierge service, to arrange flights to and from Saint-Barthélemy, VIP Service, shopping delivered to the villa prior to arrival, babysitting, restaurant reservations, car rental, in-villa personal coach, etc.
						</p>
						<p>
							The Club also provides storage for all Members. Personal belongings can be left in special containers in the hotel’s secure air-conditioned storage facility. The boxes will be delivered to the villa prior to arrival and taken away at the end of the stay. Beach Club: umbrellas, beach towels, snorkelling gear, water activities, beverage services and reserved beach chairs.
						</p>
						<h2 class="hd-medium f30">2.0 Restrictions for Direction</h2>
						<p>
							The Club provides a full time concierge service, to arrange flights to and from Saint-Barthélemy, VIP Service, shopping delivered to the villa prior to arrival, babysitting, restaurant reservations, car rental, in-villa personal coach, etc.
						</p>
						<p>
							The Club also provides storage for all Members. Personal belongings can be left in special containers in the hotel’s secure air-conditioned storage facility. The boxes will be delivered to the villa prior to arrival and taken away at the end of the stay. Beach Club: umbrellas, beach towels, snorkelling gear, water activities, beverage services and reserved beach chairs.
						</p>
						<h2 class="hd-medium f30">3.0 Conditions for Hosts</h2>
						<p>
							The Club provides a full time concierge service, to arrange flights to and from Saint-Barthélemy, VIP Service, shopping delivered to the villa prior to arrival, babysitting, restaurant reservations, car rental, in-villa personal coach, etc.
						</p>
						<h2 class="hd-medium f30">4.0 Magic</h2>
						<p>
							The Club provides a full time concierge service, to arrange flights to and from Saint-Barthélemy, VIP Service, shopping delivered to the villa prior to arrival, babysitting, restaurant reservations, car rental, in-villa personal coach, etc.
						</p>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m2">
					<a href="./" class="hd-medium f44 white-text">الصفحة الرئيسية</a>
					<a href="javascript:;" class="hd-medium f44 white-text">معلومات عنا</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أماكن الإقامة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">المطاعم</a>
					<a href="javascript:;" class="hd-medium f44 white-text">مرافق</a>
					<a href="javascript:;" class="hd-medium f44 white-text">خدمات</a>
					<a href="javascript:;" class="hd-medium f44 white-text">اتصل بنا</a>
				</div>
				<div class="text-right col m2">
					<a href="javascript:;" class="hd-medium f44 white-text">خبرة</a>
					<a href="javascript:;" class="hd-medium f44 white-text">أحداث</a>
					<a href="javascript:;" class="hd-medium f44 white-text">رجال الإعلام</a>
					<div class="newsletter">
						<p class="white-text f18 a-light">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light" required>
									<label for="email" data-error="خاطئ"></label>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>